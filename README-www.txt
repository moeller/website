Updating the blends websites
============================

The www/ folder is removed and rebuilt when changes are made. Please make your
changes in the www-src/ folder instead.

In order to build you changes you will need to install the jekyll package:

   apt-get install jekyll

After you have made your changes, in the www-src folder run:

   jekyll build

This will update the latest version of the static website in the www folder
ready to be pushed to the Debian static mirrors.

Adding a new blend
------------------

Each blend sub-site has a layout in the _layouts directory. You can copy the
template.html file there to <blend-name>.html. Each blend also has an
independent navigation menu in _includes. You can copy the template_menu.html
file to <blend-name>_menu.html. Inside the template directory you can see
an example home page you can use.

