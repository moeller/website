#!/bin/sh
./aging_packages_of_blend_pubmirror debian-med
# remove info about packages we have just checked and realised that we
# can not do much about it to update it.  Responsible maintainers are
# informed (in most cases)
grep -v \
        -e "^ psignifit "      \
        -e "^ cfflib "         \
        -e "^ pyxid "          \
        -e "^ medicalterms "   \
        -e "^ python-clips "   \
        -e "^ imview "         \
        debian-med.out
