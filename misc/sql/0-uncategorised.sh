#!/bin/sh
# seek for packages maintained by a Blends team which is not mentioned in any task

if [ $# -lt 1 ] ; then
    echo "Usage: $0 <blend>"
    exit 1
fi

case "$1" in
    debian-science)
        team="'debian-science-maintainers@lists.alioth.debian.org','pkg-scicomp-devel@lists.alioth.debian.org','pkg-electronics-devel@lists.alioth.debian.org'"
        ignore="'cbp2make','coinor-cgl','coinor-osi','coinutils',
                'dh-r','dijitso',
                'dune-common', 'dune-geometry', -- Dependencies of dune-grid
                'giella-core',
                'gmp',                          -- very basic, used in lots of other packages as dependency
                'imview-doc', 'instant',
                'libosmocore',
                'lua-torch-cwrap','lua-torch-dok','lua-torch-paths','lua-torch-sundown',
                'kwwidgets',
                'mpi-defaults',
                'openbsc','openggsn','osmo-bts','osmo-trx','libosmo-abis','libosmo-netif','libosmo-sccp','libsmpp34',
                'polyml','python-pyorick',
                'r-cran-formatr','r-cran-httpuv','r-cran-gsl','r-cran-jsonlite','r-cran-learnbayes','r-cran-lubridate',
                'r-cran-markdown','r-cran-matrixcalc','r-cran-maxlik','r-cran-mime','r-cran-misctools',
                'r-cran-pbdzmq','r-cran-polyclip','r-cran-polycub',
                'r-cran-r6','r-cran-randomfields','r-cran-repr','r-cran-rjson','r-cran-rjsonio','r-cran-rprotobuf',
                'r-cran-tensor','r-cran-uuid','r-cran-yaml',
                'ros-actionlib','ros-angles','ros-bloom','ros-bond-core','ros-catkin','ros-catkin-pkg','ros-class-loader',
                'ros-cmake-modules','ros-common-msgs','ros-dynamic-reconfigure','ros-eigen-stl-containers',
                'ros-gencpp','ros-genlisp','ros-genmsg','ros-genpy','ros-geometric-shapes','ros-geometry',
                'ros-geometry-experimental','ros-image-common','ros-interactive-markers','ros-laser-geometry',
                'ros-message-generation','ros-message-runtime','ros-navigation-msgs','ros-nodelet-core',
                'ros-pcl-conversions','ros-pcl-msgs','ros-pluginlib','ros-python-qt-binding','ros-random-numbers',
                'ros-resource-retriever','ros-robot-model','ros-ros-comm','ros-ros-comm-msgs','ros-rosconsole-bridge',
                'ros-ros','ros-roscpp-core','ros-rosdep','ros-rosdistro','ros-rosinstall','ros-rosinstall-generator',
                'ros-roslisp','ros-rospack','ros-rospkg','ros-rviz','ros-std-msgs','ros-vcstools','ros-vision-opencv',
                'ros-wstool',
                'vmtk',		-- is mentioned in Debian Med; there is no really good fitting task in Debian Science
                'xmds-doc',
                'wxastrocapture',
                'yp-svipc',
                'yorick-av',
                'yorick-curses',
                'yorick-gl',
                'yorick-gy',
                'yorick-hdf5',
                'yorick-imutil',
                'yorick-ml4',
                'yorick-mpeg',
                'yorick-mira',	-- belongs to Debian Astro and should not be listed in Debian Science
                'yorick-optimpack',
                'yorick-soy',
                'yorick-ygsl',
                'yorick-yeti',
                'yorick-ynfft',
                'yorick-yutils',
                'yorick-z'"
        ;;
    debian-med)
        team="'debian-med-packaging@lists.alioth.debian.org'"
        # these source packages are simple preconditions which should not appear in any task
        ignore="'bitops',
                'catools','charls','circos-tools','colt','ctn-doc',
                'datatables.js','dichromat',
                'f2j',
                'g2','gtable',
                'igraph',
                'jai-core','jai-imageio-core','jam-lib','jlapack',
                'labeling','libcereal','libcolt-free-java','libgzstream','libhac-java','libhat-trie','libhpptools',
                'libjung-free-java','libkmlframework-java','liblemon','liboptions-java','libquazip','librandom123',
                'librg-exception-perl','libsis-base-java','libsis-jhdf5-java','libtecla','libundead','libzeep','libzstd',
                'metaphlan2-data','metastudent-data','metastudent-data-2',
                'mgltools-bhtree','mgltools-cadd','mgltools-cmolkit','mgltools-dejavu','mgltools-geomutils','mgltools-gle','mgltools-mglutil','mgltools-molkit','mgltools-networkeditor',
                'mgltools-opengltk','mgltools-pmv','mgltools-pyautodock','mgltools-pybabel','mgltools-pyglf','mgltools-scenario2','mgltools-sff','mgltools-support','mgltools-symserv',
                'mgltools-utpackages','mgltools-viewerframework','mgltools-vision','mgltools-visionlibraries','mgltools-volume','mgltools-webservices',
                'mpj','mtj','mummy','mypy',
                'netlib-java',
                'parafly', 'permute','pixelmed-codec','pp-popularity-contest','pvrg-jpeg','pylibtiff','pyqi','python-avro','python-qcli',
                'pythonqt','python-burrito','python-bz2file','python-dictobj','python-matplotlib-venn',
                'python-rdflib-jsonld','python-schema-salad','python-sqlsoup','python3-typed-ast','python-xopen',
                'qsopt-ex',
                'r-bioc-biocinstaller','r-bioc-biocparallel','r-bioc-interactivedisplaybase','r-bioc-s4vectors','r-bioc-summarizedexperiment',
                'r-cran-adegraphics','r-cran-assertthat',
                'r-cran-backports','r-cran-batchjobs','r-cran-bbmisc','r-cran-bbmle','r-cran-bold','r-cran-biasedurn','r-cran-blockmodeling','r-cran-brew',
                'r-cran-cairo','r-cran-calibrate','r-cran-catools','r-cran-checkmate','r-cran-combinat','r-cran-crayon','r-cran-crul','r-cran-curl',
                'r-cran-data.table','r-cran-dbitest','r-cran-deal','r-cran-doparallel','r-cran-dplyr','r-cran-ellipse','r-cran-etm',
                'r-cran-evaluate','r-cran-fail','r-cran-fastcluster','r-cran-fastmatch','r-cran-filehash','r-cran-futile.logger','r-cran-futile.options','r-cran-future',
                'r-cran-g.data','r-cran-genabel.data','r-cran-ggplot2','r-cran-globals','r-cran-googlevis','r-cran-gridbase', 'r-cran-gtable',
                'r-cran-hexbin','r-cran-htmltools','r-cran-htmlwidgets','r-cran-httpcode','r-cran-httr','r-cran-hwriter','r-cran-igraph','r-cran-irlba',
                'r-cran-lambda.r','r-cran-lazyeval','r-cran-listenv','r-cran-luminescence',
                'r-cran-magrittr','r-cran-matrixstats','r-cran-memoise','r-cran-minpack.lm','r-cran-munsell',
                'r-cran-nmf','r-cran-nnls','r-cran-openssl','r-cran-optparse',
                'r-cran-pkgmaker','r-cran-plogr','r-cran-plyr','r-cran-png','r-cran-praise','r-cran-prettyunits','r-cran-princurve','r-cran-progress','r-cran-proto',
                'r-cran-r.cache','r-cran-r.methodss3','r-cran-r.oo','r-cran-r.utils','r-cran-raster','r-cran-rcurl','r-cran-registry',
                'r-cran-reshape','r-cran-reshape2','r-cran-rglwidget','r-cran-ritis','r-cran-rlumshiny','r-cran-rngtools','r-cran-rredlist','r-cran-rsolnp','r-cran-rsqlite',
                'r-cran-scales','r-cran-scatterd3','r-cran-segmented','r-cran-sendmailr','r-cran-shape',
                'r-cran-shiny','r-cran-shinybs','r-cran-solrium','r-cran-sourcetools','r-cran-stringi','r-cran-stringr',
                'r-cran-taxize','r-cran-testit','r-cran-testthat','r-cran-tibble','r-cran-tidyr','r-cran-tikzdevice','r-cran-triebeard','r-cran-truncnorm',
                'r-cran-urltools','r-cran-vioplot','r-cran-withr','r-cran-worrms','r-cran-xml2',
                'snappy1.0.3-java','socket++','spdlog','sphinxcontrib-autoprogram','yaggo'
               "
        ;;
    debian-gis)
        team="'pkg-grass-devel@lists.alioth.debian.org','pkg-osm-maint@lists.alioth.debian.org'"
        ignore="'ruby-narray-miss'"
        ;;
    debian-astro)
        team="'debian-astro-maintainers@lists.alioth.debian.org'"
        ignore="''"
        ;;
    debichem)
        team="'debichem-devel@lists.alioth.debian.org'"
        ignore="'cclib-data',
                'garlic-doc',
                'r-other-iwrlar'"
        ;;
    *)
        echo "Unsupported Blend $1"
        exit 1
        ;;
esac

if [ ! $(which psql) ] ; then
    cat <<EOT
No PostgreSQL client providing /usr/bin/psql installed.
On a Debian system please
    sudo apt-get install postgresql-client-common
EOT
    exit 1
fi

SERVICE="service=udd"
#if there is a local UDD clone just use this
if psql $PORT -l 2>/dev/null | grep -qw udd ; then
    SERVICE=udd
fi

# Check UDD connection
if ! psql $PORT $SERVICE -c "" 2>/dev/null ; then
    echo "No local UDD found, use public mirror."
    PORT="--port=5432"
    export PGPASSWORD="public-udd-mirror"
    SERVICE="--host=public-udd-mirror.xvm.mit.edu --username=public-udd-mirror udd"
fi

psql $SERVICE >$1.out <<EOT
SELECT t.source, s.bin, u.changed_by FROM (
  SELECT source FROM (
    SELECT DISTINCT source, CASE WHEN b.package IS NULL THEN 0 ELSE 1 END AS is_in_task FROM packages p
      LEFT JOIN blends_dependencies b ON p.package = b.package
      WHERE maintainer_email IN ($team)
        AND release = 'sid'                                 -- restrict to packages in unstable for the moment
        AND (b.dependency IS NULL OR b.dependency != 'i')
        AND (b.blend      IS NULL OR b.blend = '$1')
        AND source != '$1'
        AND source NOT IN ($ignore)
    ) tmp
    GROUP BY source
    HAVING MAX(is_in_task) = 0
  ) t
  LEFT JOIN (SELECT source, bin, row_number() OVER (PARTITION BY source ORDER BY version DESC) AS sver FROM sources WHERE release = 'sid') s ON t.source = s.source
  LEFT JOIN (SELECT source, changed_by, row_number() OVER (PARTITION BY source ORDER BY date DESC) AS udate FROM upload_history) u ON u.source = s.source
  WHERE s.sver = 1 and u.udate = 1
  ORDER BY source
  ;
EOT


exit 0

