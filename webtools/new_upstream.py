#!/usr/bin/python
# -*- coding: utf-8 -*-
# Check for packages of Blend which are not up to date and send
# E-Mail to Blend developer list

from sys import argv, exit, stderr

from blendstasktools import Tasks

if len(argv) <= 1:
    stderr.write("Usage: %s <Blend name>\n       The <Blend name> needs a matching config file webconf/<Blend name>.conf\n"
                 % argv[0])
    exit(-1)

tasks  = Tasks(argv[1])
if tasks.data['pkglist'] == '':
    stderr.write("Config file webconf/%s.conf is lacking pkglist field.\n" % argv[1])
    exit(-1)
tasks.GetAllDependencies(source=True)
packages = tasks.GetUpdatablePackages(dependencystatus=['official_high', 'official_low', 'non-free', 'experimental'])

for task in packages.keys():
    print("Updatable packages in Task {0}".format(task))
    for pkg_v_o in packages[task]:
        printstring = '\t{name}:\n'
        printstring += '\t\tHighest version in Debian is {debian_version}\n'
        printstring += '\t\tUpstream has {upstream_version}\n'
        printstring += '\t\tMaintainer is {maintainer[name]} <{maintainer[email]}>'
        if 'uloader' in pkg_v_o:
            printstring += '\n\t\tLast uploader was {uploader[name]} <{uploader[email]}>'
        print(printstring.format(**pkg_v_o))

# Perhaps we should also send a mail to pts@qa.debian.org
#     keyword %(pkg) %(list) = bts bts-control upload-source katie-other summary default cvs ddtp derivatives contact
# to make sure the mailing list gets full information about packages including new upstream etc
# see http://www.debian.org/doc/manuals/developers-reference/resources.html#pkg-tracking-system
