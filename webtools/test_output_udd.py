#!/usr/bin/python3

import sys
from os import listdir, makedirs
from os.path import isfile, isdir, join

from html.parser import HTMLParser
from html.entities import name2codepoint
import difflib
import re

from blendstasktools_udd import ReadConfig

s = 0  # for --sloppy flag

# parse the HTML Output
class BlendsHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self, convert_charrefs=True)
        self.tmp = ''
        self.data = []
        self.flag = 0

    def handle_starttag(self, tag, attrs):
        global s
        # self.flag = 1, for the timestamp
        if tag == 'address':
            self.flag = 1
        self.tmp = self.tmp + " <start_tag> : %s" %tag.strip()
        for attr in attrs:
            # self.flag = 1, for entire class publshed
            if s == 1:
                if attr[1] == 'published' or attr[1] == 'journal' or attr[1] == 'year':
                    self.flag = 1

            self.tmp = self.tmp + " <attr> : %s" %str(attr).strip()

    def handle_endtag(self, tag):
        self.tmp = self.tmp + " <end_tag> : %s" %tag.strip()
        # if self.flag == 1, do not append to list
        # hence diff for this will not be calculated
        # for class 'published' and for timestamp
        if self.flag == 0:
            self.data.append(self.tmp)
        elif self.flag == 1:
            self.flag = 0
        self.tmp = ''

    def handle_data(self, data):
        data = data.strip()
        # workaround to ignore fake diffs in the end like: 
        # українська (ukrajins'ka), украї нська (ukrajins'ka)
        if data != '' and data[-1] == ')':
            data = ''.join(data.split())
        self.tmp = self.tmp + " <data> : %s" %data

    def handle_comment(self, data):
        self.tmp = self.tmp + " <comment> : %s" %data.strip()

    def handle_decl(self, data):
        self.tmp = self.tmp + " <decl> : %s" %data.strip()


# check the differencce in the task files
def check_diff(blend, tasks_files, outputdir):

    for f in tasks_files:
        fp = open(outputdir+'/tasks/'+f)
        old_html = fp.read()
        fp.close()

        fp = open(outputdir+'/tasks_udd/'+f)
        new_html = fp.read()
        fp.close()
        
        tmp_dir = './tests/' + blend + "/"
        if not  isdir(tmp_dir):
            makedirs(tmp_dir)

        # create a .diff file which contains the differences
        tmp_diff = tmp_dir + f + '.diff'

        # final_old_data is a list containing parsed old task files data
        parser1 = BlendsHTMLParser()
        parser1.feed(old_html)
        old_data = parser1.data
        final_old_data = []
        for od in old_data:
            if od != '':
                tmp = " ".join(od.split())
            if tmp != ' ' or tmp != '' or tmp != '\n':    
                final_old_data.append(tmp)

        # final_new_data is a list containing parsed new task files data
        parser2 = BlendsHTMLParser()
        parser2.feed(new_html)
        new_data = parser2.data
        final_new_data = []
        for nd in new_data:
            if nd != '':
                tmp = " ".join(nd.split())
            if tmp != ' ' or tmp != '' or tmp != '\n':
                final_new_data.append(tmp)

        if final_old_data == final_new_data:
           print("Both the task files are identical") 
        
        fp = open(tmp_diff,'w')
        lines = []
        for line in difflib.unified_diff(final_old_data, final_new_data, fromfile='tasks/'+f, tofile='tasks_udd/'+f, lineterm=""):
            line = line + '\n'
            lines.append(line)
        fp.writelines(lines)
        fp.close()


# get the names of tasks files in dir_path
def task_file(dir_path, lang=''):
    tasks_files = []
    if isdir(dir_path):
        for f in listdir(dir_path):
            if not f.endswith('.html'):
                continue
            if isfile(join(dir_path, f)):
                tmp = f.split('.')
                # get names of task files which are in english
                if tmp[1] == 'en':
                    tasks_files.append(f)
                if lang != '' and lang != 'en':
                    if tmp[1] == lang:
                        tasks_files.append(f)
            else:
                print("%s is not a file " %f)
    else:
        print("%s is not a directory" %dir_path)
    return tasks_files


def main():
    global s
    if len(sys.argv) <= 1:
        print("Usage: ./test_output.py <optional sloppy flag (-s)> <Blend> <optional lang>\nOutput directory : ./tests/<Blend>/<task_file.diff>")
        exit(-1)

    blend = ''  # to store name of blend
    lang = '' # for diff in additional lang

    if sys.argv[1] == '-s':
        s = 1
        blend = sys.argv[2]
        if len(sys.argv) == 4:
            lang = sys.argv[3]
        elif len(sys.argv) == 3:
            lang = ''
        else:
            print("Usage: ./test_output.py <optional sloppy flag (-s)> <Blend> <optional lang>\nOutput directory : ./tests/<Blend>/<task_file.diff>")
            exit(-1)
    else:
        s = 0
        blend = sys.argv[1]
        if len(sys.argv) == 3:
            lang = sys.argv[2]
        elif len(sys.argv) == 2:
            lang = ''
        else:
            print("Usage: ./test_output.py <optional sloppy flag (-s)> <Blend> <optional lang>\nOutput directory : ./tests/<Blend>/<task_file.diff>")
            exit(-1)

    data = ReadConfig(blend)
    outputdir = data['outputdir']

    # get names of old task files
    old_tasks_files = task_file(outputdir + '/tasks', lang)
    # get names of new task files
    new_tasks_files = task_file(outputdir + '/tasks_udd', lang)

    # check whether the tasks files created are same or not
    # irrespective of order of the two lists
    if set(old_tasks_files) == set(new_tasks_files):
        check_diff(blend, new_tasks_files, outputdir)
    else:
        print('Tasks files created are not the same')


if __name__ == '__main__':
    main()
